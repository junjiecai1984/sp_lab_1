/* Written by JunjieCai
 * 2020-04-15
 *
 * This file contains many utilities such as open and close files
 * */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXNAME  64
#define MAXEMP  1024

struct employee {
    char name[MAXNAME], surname[MAXNAME];
    int id, salary;
};

int comparator(const void *p, const void *q)
{
    int l = ((struct employee *)p)->id;
    int r = ((struct employee *)q)->id;
    return (l - r);
}

void sort_employee_by_id(struct employee *employee_list){
    int size = sizeof(employee_list) / sizeof(employee_list[0]);
    qsort((void*)employee_list, size, sizeof(employee_list[0]), comparator);

}

int open_file(char *fname) {
    /* open file */
    if (fopen(fname, "r") == NULL)
        return -1;
    return 0;
}

int read_int(int *address) {
    /* read int into address */
    if (scanf("%d", address) == 0)
        return -1;
    return 0;
}

int read_float(int *address) {
    /* read int into float */
    if (scanf("%f", address) == 0)
        return -1;
    return 0;
}

int read_string(char *address) {
    /* read int into string */
    if (scanf("%s", address) == 0)
        return -1;
    return 0;
}

void close_file(const FILE *fp) {
    /* close file by pointed by fp */
    fclose(fp);
}



int binary_search(const int array[], int l, int r, int x) {
    /* binary search for integer */
    if (r >= l) {
        int mid = l + (r - l) / 2;
        if (array[mid] == x)
            return mid;
        if (array[mid] > x)
            return binary_search(array, l, mid - 1, x);
        return binary_search(array, mid + 1, r, x);
    }
    return -1;
}

void print_by_key(struct employee employee_list[MAXNAME], int key) {
    /* binary search for integer */
    printf("----------------------------------------------\n");
    printf(" NAME                       SALARY      ID\n");
    printf(" %-10s %-10s %10d %10d \n", employee_list[key].name, employee_list[key].surname, employee_list[key].salary,
           employee_list[key].id);
    printf("----------------------------------------------\n");
}

int search_by_last_name(struct employee employee_list[MAXEMP], int n_emp, char surname[MAXNAME]) {
    /* return index when found, else return -1 */
    for (int i = 0; i < n_emp; i++) {
        if (strcmp(employee_list[i].surname, surname) == 0) {
            return i;
        }
    }
    return -1;
}

