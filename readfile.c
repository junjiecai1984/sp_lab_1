/* Written by JunjieCai
 * 2020-04-15
 *
 * This file contains the main function and related helper functions
 * */

#include <stdio.h>
#include <string.h>
#include "readfile.h"

int _delete_employee_command(const struct employee *employee_list, int n_emp);

void _update_employee_command(const struct employee *employee_list, int n_emp);

void _display_menu() {
    /* Display menu to users */
    printf("**********************************\n");
    printf("Employee DB Menu:\n");
    printf("**********************************\n");
    printf(" (1) Print the Database\n");
    printf(" (2) Lookup by ID\n");
    printf(" (3) Lookup by Last Name\n");
    printf(" (4) Add an Employee\n");
    printf(" (5) Quit\n");
    printf(" (6) Remove an employee\n");
    printf(" (7) Update an employee's information\n");
    printf(" (8) Print the M employees with the highest salaries\n");
    printf(" (9) Find all employees with matching last name\n");
    printf("**********************************\n");
}

int _get_option() {
    /* Get option from user */
    printf("Enter your option: ");
    int option;
    scanf("%d", &option);
    return option;
}

void _display_all(const struct employee *employee, int n_emp) {
    /* Display all employee as table format */
    printf("----------------------------------------------\n");
    printf(" NAME                       SALARY      ID\n");
    printf("----------------------------------------------\n");
    for (int i = 0; i < n_emp; i++)
        printf(" %-10s %-10s %10d %10d \n", employee[i].name, employee[i].surname, employee[i].salary,
               employee[i].id);
    printf("----------------------------------------------\n");
}

int _find_employee_by_id(const struct employee *employee, int emp_id, int n_emp) {
    /* Search employee by id and display information */

    int key;

    int list[MAXEMP];
    for (int i = 0; i < n_emp; i++) {
        list[i] = employee[i].id;
    }
    key = binary_search(list, 0, n_emp, emp_id);
    if (key == -1)
        printf("Employee with id %d not found in DB\n", emp_id);
    else
        print_by_key(employee, key);

    return key;
}

int _find_employee_by_last_name(const struct employee *employee, int n_emp) {
    /* Search employee by last_name and display information */
    char surname[MAXNAME];

    printf("Enter Employee's last name (no extra spaces): ");
    scanf(" %s", &surname);
    int key;
    key = search_by_last_name(employee, n_emp, surname);
    if (key == -1)
        printf("Employee with last name %s not found in DB\n", surname);
    else
        print_by_key(employee, key);
}

int _load_data(const char *fname, struct employee *employee) {
    /* Read data from file and save values into struct array */
    FILE *fp = fopen(fname, "r");
    int n_emp = 0;
    while (!feof(fp)) {
        fscanf(fp, "%d %s %s %d\n", &employee[n_emp].id, &employee[n_emp].name, &employee[n_emp].surname,
               &employee[n_emp].salary);
        n_emp++;
    }
    close_file(fp);

    sort_employee_by_id(employee);
    return n_emp;
}

int _add_employee(struct employee *employee, int n_emp) {
    /* Get input from user and put new employee into struct array */
    int salary, confirm;
    printf("Enter the first name of the employee: ");
    char name[MAXNAME];
    read_string(name);

    printf("Enter the last name of the employee: ");
    char surname[MAXNAME];
    read_string(surname);

    printf("Enter employee's salary (30000 to 150000): ");
    read_int(&salary);
    printf("do you want to add the following employee to the DB? \n");
    printf("%s %s, salary: %d \n", name, surname, salary);
    printf("Enter 1 for yes, 0 for no: ");
    read_int(&confirm);
    if (confirm == 1) {
        if ((salary >= 30000) && (salary <= 150000)) {
            int cur_id = employee[n_emp - 1].id;
            int new_id = cur_id + 1;
            strcpy(employee[n_emp].name, name);
            strcpy(employee[n_emp].surname, surname);
            employee[n_emp].salary = salary;
            employee[n_emp].id = new_id;
            n_emp++;
        } else {
            printf("Salary range invalid. Please enter a value between 30000 and 150000\n");
        }
    }
    return n_emp;
}

int _delete_employee(struct employee *employee_list, int key, int n_emp) {
    struct employee new_employee_list[MAXNAME];

    for (int i = 0; i < n_emp; i++) {
        strcpy(new_employee_list[i].name, employee_list->name);
        strcpy(new_employee_list[i].surname, employee_list->surname);
        new_employee_list[i].salary = employee_list->salary;
        new_employee_list[i].id = employee_list->id;
        employee_list++;
    }
    for (int i = 0; i < n_emp; i++) {
        employee_list--;
    }
    for (int i = 0; i < n_emp - 1; i++) {
        if (i >= key) {
            employee_list->salary = new_employee_list[i + 1].salary;
            employee_list->id = new_employee_list[i + 1].id;
            strcpy(employee_list->name, new_employee_list[i + 1].name);
            strcpy(employee_list->surname, new_employee_list[i + 1].surname);
        }
        employee_list++;
    }
    for (int i = 0; i < n_emp - 1; i++) {
        employee_list--;
    }
}


void _update_by_key(struct employee *employee_list, int key, char name[], char surname[], int salary) {
    for (int i = 0; i < key; i++) {
        employee_list++;
    }
    strcpy(employee_list->name, name);
    strcpy(employee_list->surname, surname);
    employee_list->salary = salary;
    for (int i = 0; i < key; i++) {
        employee_list--;
    }
}

int _get_largest(struct employee employee_list[MAXEMP], int n_emp) {
    int largest = 0;

    for (int i = 1; i < n_emp; i++) {
        if (employee_list[i].salary > employee_list[largest].salary) {
            largest = i;
        }
    }
    return largest;
}


int _find_all_last_name(struct employee employee_list[MAXEMP], int n_emp, char surname[MAXNAME]) {
    int flag = search_by_last_name(employee_list, n_emp, surname);

    if (flag == -1) {
        printf("EMployee with last name %s not found in DB \n", surname);
    } else {
        printf("NAME\t\tSURNAME\t\tSalary\t\tID\n");
        printf("------------------------------\n");
        for (int i = 0; i < n_emp; i++) {
            if (strcasecmp(employee_list[i].surname, surname) == 0) {
                printf("%s\t\t%s\t\t%d\t\t%d\n", employee_list[i].name, employee_list[i].surname,
                       employee_list[i].salary, employee_list[i].id);
                printf("------------------------------\n");
            }
        }

    }
}

int main(int argc, char *argv[]) {
    /* Display menu to users and perform actions */
    if (argc < 2) {
        printf("Pass filename to read from...\n");
        return 0;
    }

    char *fname = argv[1];
    if (open_file(fname) == -1) {
        printf("Error reading file");
        return -1;
    }

    struct employee employee_list[MAXNAME];
    int n_emp = _load_data(fname, employee_list);

    while (1) {
        _display_menu();

        int option = _get_option();

        if (option == 1) {
            _display_all(employee_list, n_emp);
        } else if (option == 2) {
            int emp_id;
            printf("Enter a 6 digit employee id: ");
            scanf(" %d", &emp_id);

            _find_employee_by_id(employee_list, emp_id, n_emp);
        } else if (option == 3) {
            _find_employee_by_last_name(employee_list, n_emp);
        } else if (option == 4) {
            n_emp = _add_employee(employee_list, n_emp);
        } else if (option == 5) {
            printf("goodbye!\n");
            break;
        } else if (option == 6) {
            n_emp = _delete_employee_command(employee_list, n_emp);
        } else if (option == 7) {
            _update_employee_command(employee_list, n_emp);
        } else if (option == 8) {
            printf("Top ? salary you want to see?\n");
            int m;
            read_int(&m);

            struct employee temp_list[MAXEMP], employee_with_largest_salary[m];
            int temp_n_emp = n_emp;

            for (int i = 0; i < n_emp; i++) {
                strcpy(temp_list[i].name, employee_list[i].name);
                strcpy(temp_list[i].surname, employee_list[i].surname);
                temp_list[i].salary = employee_list[i].salary;
                temp_list[i].id = employee_list[i].id;
            }

            for (int i = 0; i < m; i++) {
                int largest = _get_largest(temp_list, temp_n_emp);
                strcpy(employee_with_largest_salary[i].name, temp_list[largest].name);
                strcpy(employee_with_largest_salary[i].surname, temp_list[largest].surname);
                employee_with_largest_salary[i].salary = temp_list[largest].salary;
                employee_with_largest_salary[i].id = temp_list[largest].id;

                _delete_employee(temp_list, largest, temp_n_emp);
                temp_n_emp--;
            }
            _display_all(employee_with_largest_salary, m);
        } else if (option == 9) {
            printf("Enter the last name of the employee: ");
            char surname[MAXNAME];
            read_string(surname);

            _find_all_last_name(employee_list, n_emp, surname);
        } else {
            printf("Hey, %d is not between 1 and 5, try again...\n", option);
            continue;
        }
    }
}

void _update_employee_command(const struct employee *employee_list, int n_emp) {
    printf("Enter a 6 digit employee id:");
    int emp_id;
    read_int(&emp_id);

    int key = _find_employee_by_id(employee_list, emp_id, n_emp);

    if (key == -1) {
        printf("Employee with id %d not found in DB\n", emp_id);
    } else {
        printf("Enter the updated first name of the employee:\n ");
        char name[MAXNAME];
        read_string(name);

        printf("Enter the updated surname of the employee:\n");
        char surname[MAXNAME];
        read_string(surname);

        printf("Enter the updated salary of the employee: ");
        int salary;
        read_int(&salary);

        printf("Do you want to update the employee with the following information?");
        printf("%s %s, salary: %d \n", name, surname, salary);

        printf("Enter 1 for yes, 0 for no:");
        int confirm;
        read_int(&confirm);

        if (confirm == 1) {
            _update_by_key(employee_list, key, name, surname, salary);
            printf("Successfully update! The updated information is");
            print_by_key(employee_list, key);
        } else {
            printf("The above employee was not updated.\n");
        }
    }
}

int _delete_employee_command(const struct employee *employee_list, int n_emp) {
    printf("Enter the ID of the employee: ");
    int id;

    read_int(&id);

    int key = _find_employee_by_id(employee_list, id, n_emp);

    if (key == -1) {
        printf("Employee not found");
    } else {
        printf("Are you sure to delete the above employee?\n Enter 1 for yes, 0 for no: ");
        int confirm;
        read_int(&confirm);

        if (confirm == 1) {
            _delete_employee(employee_list, key, n_emp);
            printf("The above employee was  deleted\n");
            n_emp--;
        } else {
            printf("The above employee was not deleted\n");
        }
    }
    return n_emp;
}
