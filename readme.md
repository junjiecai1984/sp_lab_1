# Readme
## How to run?
The file **readfile** is compiled version of my program. 

The steps to run it:
1. Open the terminal
2. First use '''cd''' to enter the directory containing **readfile** 
3. Run '''./readfile'''

## How to build?
The **readfile.h** and **readfile.h** contains the source code and can be build and compiled. (The details depends on the enviroemnt)

## Design
This is the diagrams (UML) to help to design in a top-down manner(Note: The fianl codes are not perfectly mapped to the desing)

![alt text](https://bitbucket.org/junjiecai1984/sp_lab_1/raw/4cf674bc3686ca75555d6ca90cf7493b23ef553b/design.png)
